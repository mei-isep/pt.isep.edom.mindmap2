/**
 */
package pt.isep.edom.mindmap2;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Topic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.isep.edom.mindmap2.Topic#getSubtopics <em>Subtopics</em>}</li>
 *   <li>{@link pt.isep.edom.mindmap2.Topic#getParent <em>Parent</em>}</li>
 *   <li>{@link pt.isep.edom.mindmap2.Topic#getDescription <em>Description</em>}</li>
 *   <li>{@link pt.isep.edom.mindmap2.Topic#getStart <em>Start</em>}</li>
 *   <li>{@link pt.isep.edom.mindmap2.Topic#getEnd <em>End</em>}</li>
 *   <li>{@link pt.isep.edom.mindmap2.Topic#getPriority <em>Priority</em>}</li>
 * </ul>
 *
 * @see pt.isep.edom.mindmap2.Mindmap2Package#getTopic()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='EndAfterStart'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot EndAfterStart='self.end &gt;= self.start'"
 * @generated
 */
public interface Topic extends MapElement {
	/**
	 * Returns the value of the '<em><b>Subtopics</b></em>' reference list.
	 * The list contents are of type {@link pt.isep.edom.mindmap2.Topic}.
	 * It is bidirectional and its opposite is '{@link pt.isep.edom.mindmap2.Topic#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subtopics</em>' reference list.
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getTopic_Subtopics()
	 * @see pt.isep.edom.mindmap2.Topic#getParent
	 * @model opposite="parent"
	 * @generated
	 */
	EList<Topic> getSubtopics();

	/**
	 * Returns the value of the '<em><b>Parent</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link pt.isep.edom.mindmap2.Topic#getSubtopics <em>Subtopics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' reference.
	 * @see #setParent(Topic)
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getTopic_Parent()
	 * @see pt.isep.edom.mindmap2.Topic#getSubtopics
	 * @model opposite="subtopics"
	 * @generated
	 */
	Topic getParent();

	/**
	 * Sets the value of the '{@link pt.isep.edom.mindmap2.Topic#getParent <em>Parent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(Topic value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getTopic_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link pt.isep.edom.mindmap2.Topic#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #setStart(Date)
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getTopic_Start()
	 * @model
	 * @generated
	 */
	Date getStart();

	/**
	 * Sets the value of the '{@link pt.isep.edom.mindmap2.Topic#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(Date value);

	/**
	 * Returns the value of the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' attribute.
	 * @see #setEnd(Date)
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getTopic_End()
	 * @model
	 * @generated
	 */
	Date getEnd();

	/**
	 * Sets the value of the '{@link pt.isep.edom.mindmap2.Topic#getEnd <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' attribute.
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(Date value);

	/**
	 * Returns the value of the '<em><b>Priority</b></em>' attribute.
	 * The literals are from the enumeration {@link pt.isep.edom.mindmap2.Priority}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority</em>' attribute.
	 * @see pt.isep.edom.mindmap2.Priority
	 * @see #setPriority(Priority)
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getTopic_Priority()
	 * @model
	 * @generated
	 */
	Priority getPriority();

	/**
	 * Sets the value of the '{@link pt.isep.edom.mindmap2.Topic#getPriority <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority</em>' attribute.
	 * @see pt.isep.edom.mindmap2.Priority
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(Priority value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='self-&gt;closure(subtopics)'"
	 * @generated
	 */
	EList<Topic> allSubtopics();

} // Topic
