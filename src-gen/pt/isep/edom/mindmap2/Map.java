/**
 */
package pt.isep.edom.mindmap2;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link pt.isep.edom.mindmap2.Map#getTitle <em>Title</em>}</li>
 *   <li>{@link pt.isep.edom.mindmap2.Map#getElements <em>Elements</em>}</li>
 *   <li>{@link pt.isep.edom.mindmap2.Map#getRootTopics <em>Root Topics</em>}</li>
 * </ul>
 *
 * @see pt.isep.edom.mindmap2.Mindmap2Package#getMap()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='mustHaveTitle'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot mustHaveTitle='not title.oclIsUndefined()'"
 * @generated
 */
public interface Map extends EObject {
	/**
	 * Returns the value of the '<em><b>Title</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Title</em>' attribute.
	 * @see #setTitle(String)
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getMap_Title()
	 * @model
	 * @generated
	 */
	String getTitle();

	/**
	 * Sets the value of the '{@link pt.isep.edom.mindmap2.Map#getTitle <em>Title</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Title</em>' attribute.
	 * @see #getTitle()
	 * @generated
	 */
	void setTitle(String value);

	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link pt.isep.edom.mindmap2.MapElement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getMap_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<MapElement> getElements();

	/**
	 * Returns the value of the '<em><b>Root Topics</b></em>' reference list.
	 * The list contents are of type {@link pt.isep.edom.mindmap2.Topic}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Topics</em>' reference list.
	 * @see pt.isep.edom.mindmap2.Mindmap2Package#getMap_RootTopics()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot derivation='let topics : Set(mindmap2::Topic) =\n            \tself.elements-&gt;select(oclIsKindOf(mindmap2::Topic))-&gt;collect(oclAsType(mindmap2::Topic))-&gt;asSet() in\n     \t\t\t\ttopics-&gt;asOrderedSet()-&gt;symmetricDifference(topics.subtopics-&gt;asSet())-&gt;asOrderedSet()'"
	 * @generated
	 */
	EList<Topic> getRootTopics();

} // Map
